package com.example.extempo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class DBCalificaciones {

    private Context context;
    private DBHelper helper;
    private SQLiteDatabase db;

    private String[] COLUMS = {
            Tables.CalificacionesTabla.calificacion,
            Tables.CalificacionesTabla.grupo,
            Tables.CalificacionesTabla.materia,
            Tables.CalificacionesTabla.matricula
    };

    public DBCalificaciones(Context context) {
        this.context = context;
        this.helper = new DBHelper(this.context);
        openDatabase();
    }

    public long insertCalificacion(Calficacion calficacion) {
        ContentValues values = new ContentValues();
        values.put(Tables.CalificacionesTabla.calificacion, calficacion.getCalficiacion());
        values.put(Tables.CalificacionesTabla.grupo, calficacion.getGrupo());
        values.put(Tables.CalificacionesTabla.materia, calficacion.getMateria());
        values.put(Tables.CalificacionesTabla.matricula, calficacion.getMatircula());

        return db.insert(Tables.CalificacionesTabla.TableName, null, values);
    }

    public ArrayList<Calficacion> getCalificaciones() {
        ArrayList<Calficacion> list = new ArrayList<>();
        Cursor cursor = db.query(Tables.CalificacionesTabla.TableName, COLUMS, null, null, null, null, null);

        if (cursor.getCount() == 0) {
            return list;
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(read(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    private Calficacion read(Cursor cursor) {
        Calficacion calficacion = new Calficacion();
        calficacion.setCalficiacion(cursor.getFloat(0));
        calficacion.setGrupo(cursor.getString(1));
        calficacion.setMateria(cursor.getString(2));
        calficacion.setMatircula(cursor.getString(3));
        return calficacion;
    }


    public void close() {
        helper.close();
    }

    public void openDatabase() {
        db = helper.getWritableDatabase();
    }


}
